<?php
     session_start();
     include '../config.php';
     include '../libs/db.php';
     include '../libs/admin.php';
     include '../libs/widgets.php';

     $view = $_GET['view'] ?? 'index';

     if(isAuthAdmin()){
          include $config['base']['path'].'views/admin_layouts/index.php';
     }
     else{
          include $config['base']['path'].'views/admin_pages/login.php';
     }