<?php
     $config = [
          'base' => [
               'url' => 'http://localhost/Courses.loc/',
               'path' => $_SERVER['DOCUMENT_ROOT'].'/Courses.loc/'
          ],
          'db' => [
               'host' => 'localhost',
               'username' => 'root',
               'userpassword' => '',
               'dbname' => 'courses_base',
               'tablePrefix' => 'courses_'
          ]
     ];
     define('url', 'http://localhost/Courses.loc/');
     define('reg', 'http://localhost/Courses.loc/Registratsiya');
     define('teacher', 'http://localhost/Courses.loc/Teachers/');
     define('students', 'http://localhost/Courses.loc/Students/');
     define('admin', 'http://localhost/Courses.loc/admin/');
