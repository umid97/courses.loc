<?php
     session_start();
     include 'config.php';
     include 'libs/db.php';
     include 'libs/widgets.php';

     $view = $_GET['view'] ?? 'index';

     include($config['base']['path'].'views/layouts/main.php');