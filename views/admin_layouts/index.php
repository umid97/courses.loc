<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <meta name="keywords" content="academy, college, coursera, courses, education, elearning, kindergarten, lms, lynda, online course, online education, school, training, udemy, university">
     <meta name="description" content="Edumy - LMS Online Education Course & School HTML Template">
     <meta name="CreativeLayers" content="ATFN">
     <!-- css file -->
     <link rel="stylesheet" href="<?=$config['base']['url'].'web/'?>admin/css/bootstrap.min.css">
     <link rel="stylesheet" href="<?=$config['base']['url'].'web/'?>admin/css/style.css">
     <!-- Responsive stylesheet -->
     <link rel="stylesheet" href="<?=$config['base']['url'].'web/'?>admin/css/responsive.css">
     <!-- Title -->
     <title>Edumy - LMS Online Education Course & School HTML Template</title>
     <!-- Favicon -->
     <link href="<?=$config['base']['url'].'web/'?>admin/images/favicon.ico" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
     <link href="<?=$config['base']['url'].'web/'?>admin/images/favicon.ico" sizes="128x128" rel="shortcut icon" />

</head>
<body>
<div class="container">
     <div class="row">
          <div class="col-12">
               <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                         <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                         <ul class="navbar-nav">
                              <li class="nav-item <?php
                                   if(isset($view) && $view == 'index') echo 'active';
                              ?>">
                                   <a class="nav-link" href="<?=admin?>">Studentlar <span class="sr-only">(current)</span></a>
                              </li>
                              <li class="nav-item <?php
                              if(isset($_GET['view']) && $_GET['view'] == 'news') echo 'active';
                              ?>">
                                   <a class="nav-link" href="<?=admin.'news'?>">Yangiliklar</a>
                              </li>
                              <li class="nav-item <?php
                              if(isset($_GET['view']) && $_GET['view'] == 'slider') echo 'active';
                              ?>">
                                   <a class="nav-link" href="<?=admin.'slider'?>">Slider</a>
                              </li>
                              <li class="nav-item <?php
                              if(isset($_GET['view']) && $_GET['view'] == 'teacher') echo 'active';
                              ?>">
                                   <a class="nav-link" href="<?=admin.'teacher'?>">O'qutuvchilar</a>
                              </li>
                         </ul>

                    </div>
               </nav>
          </div>
     </div>
</div>
<div class="container">
     <div class="row">
          <?php
               include($config['base']['path'].'views/admin_pages/'.$view.'.php');
          ?>
     </div>
</div>
<!-- Wrapper End -->
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/jquery-3.3.1.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/jquery-migrate-3.0.0.min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/popper.min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/jquery.mmenu.all.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/ace-responsive-menu.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/isotop.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/snackbar.min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/simplebar.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/parallax.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/scrollto.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/jquery-scrolltofixed-min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/jquery.counterup.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/wow.min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/progressbar.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/slider.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/timepicker.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/script.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/ajax.js"></script>
</body>

<!-- Mirrored from grandetest.com/theme/edumy-html/page-dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 15 Jan 2020 07:25:56 GMT -->
</html>
