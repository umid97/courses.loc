<div class="main_content_container">
    <table class="table table-hover table-bordered">
        <thead>
        <th>#</th><th>Student fish</th><th>Telefon</th><th>File</th><th>Text</th><th></th><th></th>
        </thead>
        <tbody>
        <?php foreach(getConditionData('chat', ['person', 'status'], [$_SESSION['login_t'], 'noactive']) as $r): ?>
            <tr>
                <td>
                    <?=$r['id']?>
                </td>
                <td>
                    <?=isGlobalCol('students', ['login'], [$r['people']], 'AND', 'fullname')?>
                </td>
                <td>
                    <?=isGlobalCol('students', ['login'], [$r['people']], 'AND', 'phone')?>
                </td>
                <td>
                    <a download href="<?=$config['base']['url'].'web/upload/'.$r['files']?>"><?=$r['files']?></a>
                </td>
                <td>
                    <?=substr($r['text'], 0, 50).'...'; ?>
                </td>
                <td>
                    <a href="<?=teacher.'one/'.$r['id']?>" class="btn btn-info btn-sm" ><i class="fa fa-eyedropper" aria-hidden="true"></i></a>
                </td>
                <td>
                    <a href="<?=teacher.'del/'.$r['id']?>" class="btn btn-danger btn-sm" ><i class="fa fa-trash" aria-hidden="true"></i></a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>