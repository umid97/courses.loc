<div class="container">
    <table class="table table-bordered">
        <thead>
            <th>#</th><th>FISH</th><th>Telefon</th><th>Manzil</th><th>Rasm</th>
        </thead>
        <?php $son = isset($_GET['id']) ? $_GET['id'] : 0; foreach(getPage('students', $son) as $r): ?>
            <tr class="<?php
            if($r['status'] == 'active'){
                echo 'alert alert-warning';
            }
            elseif($r['status'] == 'inactive'){
                echo 'alert alert-danger';
            }
            elseif($r['status'] == 'noactive'){
                echo 'alert alert-success';
            }
            ?>">
                <td><?=$r['id']?></td>
                <td>
                    <a href="<?=teacher.'person/'.$r['id'];?>" class="text-primary"><?=$r['fullname']?></a>
                </td>

                <td><?=$r['phone']?></td>

                <td><?=$r['address']?></td>

                <td>
                    <img src="<?=$config['base']['url'].'web/students_img/'.$r['img']?>" class="img-responsive" alt="">
                </td>


            </tr>



        <?php endforeach; ?>
        <?=Pagination('students', teacher, 'mystudents');?>
    </table>
</div>