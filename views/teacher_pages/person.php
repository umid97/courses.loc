<div class="main_content_container">
    <table class="table table-hover">
        <a href="<?=teacher.'mystudents'?>" class="btn btn-info btn-sm"><i class="fa fa-reply" aria-hidden="true"></i></a>
        <thead>
        <th>#</th><th>FISH</th><th>Manzil</th><th>Rasm</th><th>Telefon nomer</th><th>Ma'lumotlar</th><th></th>
        </thead>
        <tbody>
        <?php foreach(getConditionData('students', ['id'], [$_GET['id']]) as $r): ?>
            <tr>
                <td><?=$r['id']?></td>
                <td><?=$r['fullname']?></td>
                <td><?=$r['address']?></td>
                <td><img src="<?=$config['base']['url'].'web/students_img/'.$r['img']?>" alt=""></td>
                <td><?=$r['phone']?></td>
                <td width="40%"><?=$r['info']?></td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>

