<?php
     if(isset($_POST['send'])){
          if($_POST['type'] === 'Ustoz'){
               if(IsAuth('teachers', ['login', 'password'], [$_POST['username'], $_POST['password']], 'AND')){
                    $_SESSION['fullname_t'] = isGlobalCol('teachers', ['login', 'password'], [$_POST['username'], $_POST['password']], 'AND', 'fullname');
                    $_SESSION['login_t'] = isGlobalCol('teachers', ['login', 'password'], [$_POST['username'], $_POST['password']], 'AND', 'login');
                    ?>
                    <script>
                         window.location = '<?=url?>Teachers/';
                    </script>
                    <?php
               }
               else{
                    ?>
                   <script>
                       alert('Topilmadi!');
                       window.location = '<?=url?>';
                   </script>
                   <?php
               }
          }
          elseif($_POST['type'] === 'Shogird'){
               if(IsAuth('students', ['login', 'password'], [$_POST['username'], $_POST['password']], 'AND')){
                    $_SESSION['fullname_s'] = isGlobalCol('students', ['login', 'password'], [$_POST['username'], $_POST['password']], 'AND', 'fullname');
                    $_SESSION['login_s'] = isGlobalCol('students', ['login', 'password'], [$_POST['username'], $_POST['password']], 'AND', 'login');
                    ?>
                    <script>
                        window.location = '<?=url?>Students/';
                    </script>
                    <?php
               }
               else{
                    echo 'Topilmadi! Students!';
               }
          }
     }
     else{
          ?>
               <script>
                   window.location = '<?=url?>Teachers/';
               </script>
          <?php
     }