<?php
     if(isset($_POST['send'])){
         if($_POST['type'] === 'Ustoz'){
              if(IsAuth('teachers', ['login', 'password'], [$_POST['login'], $_POST['password']], 'AND')){
                   $_SESSION['error'] = 'Login yoki parolni qayta kiriting.';
                   ?>
                  <script>
                      window.location = '<?=url."Registratsiya";?>';
                  </script>
                   <?php
              }
              else{
                   $name = getArrayExplode($_FILES['img']['name'], '.');
                   move_uploaded_file($_FILES['img']['tmp_name'], $config['base']['path'].'web/teachers_img/'.$name);
                   if(InsertGlobal('teachers', ['fullname', 'phone', 'address', 'img', 'info', 'login', 'password', 'status'], [$_POST['fullname'], $_POST['phone'], $_POST['address'], $name, $_POST['info'], $_POST['login'], $_POST['password'], 'active'])){
                        $_SESSION['fullname_t'] = $_POST['fullname'];
                        $_SESSION['login_t'] = $_POST['login'];
                        unset($_SESSION['error']);
                        ?>
                       <script>
                           window.location = 'Teachers/'
                       </script>
                        <?php
                   }
                   else{
                        ?>
                       <script>
                           alert('Xatolik');
                           window.location = '<?=url.'Registratsiya'?>'
                       </script>
                       <?php
                   }
              }
         }
         elseif($_POST['type'] === 'Shogird'){
              if(IsAuth('students', ['login', 'password'], [$_POST['login'], $_POST['password']], 'AND')){
                   $_SESSION['error'] = 'Login yoki parolni qayta kiriting.';
                   ?>
                  <script>
                      window.location = '<?=url."Registratsiya";?>';
                  </script>
                   <?php
              }
              else{
                   $name = getArrayExplode($_FILES['img']['name'], '.');
                   move_uploaded_file($_FILES['img']['tmp_name'], $config['base']['path'].'web/students_img/'.$name);
                   if(InsertGlobal('students', ['fullname', 'phone', 'address', 'img', 'info', 'login', 'password', 'status'], [$_POST['fullname'], $_POST['phone'], $_POST['address'], $name, $_POST['info'], $_POST['login'], $_POST['password'], 'active'])){
                        $_SESSION['fullname_s'] = $_POST['fullname'];
                        $_SESSION['login_s'] = $_POST['login'];
                        unset($_SESSION['error']);
                        ?>
                       <script>
                           window.location = 'Students/'
                       </script>
                        <?php
                   }
                   else{
                        echo 'Xatolik!';
                   }
              }
         }

     }


