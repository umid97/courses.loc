<?php if(isset($_GET['id'])): ?>
     <div class="container">
          <div class="row">

               <?php foreach(getConditionData('teachers', ['id'], [$_GET['id']]) as $row): ?>
                    <div class="col-sm-4">
                        <img src="<?=$config['base']['url'].'web/teachers_img/'.$row['img'];?>" alt="" class="thumbnail" width="100%">
                    </div>
                    <div class="col-sm-8  alert alert-danger">
                        <p class="alert alert-danger alert-link"><?=$row['phone']?></p>
                        <p class="alert alert-danger">
                             <?=$row['info'];?>
                        </p>
                         <p class="alert alert-danger alert-link"><?=$row['address']?></p>
                         <p class="alert alert-danger">
                             Shaxsiy kabineting orqali muloqat qilishingiz mumkin!
                         </p>
                    </div>
               <?php endforeach; ?>
          </div>
     </div>
<?php else: ?>

<?php endif; ?>