<section id="contact_parallax" style='background-image: url("<?=$config['base']['url'].'web/'?>upload/contact_parallax.png")' class="section-white page-title-wrapper" data-stellar-background-ratio="1" data-stellar-offset-parent="true">
     <div class="container">
          <div class="relative">
               <div class="row">
                    <div class="col-md-12">
                         <div class="section-title text-center">
                              <h4>Ro'yxatdan o'tish</h4>
                              <hr>
                              <ol class="breadcrumb">
                                   <li><a href="<?=url?>">Bosh sahifa</a></li>
                                   <li class="active">Ro'yxatdan o'tish</li>
                              </ol>
                         </div><!-- end title -->
                    </div><!-- end col -->
               </div><!-- end row -->
          </div><!-- end relative -->
     </div><!-- end container -->
</section><!-- end section-white -->

<section class="section-white">
     <div class="container">
          <div class="row">
               <div class="col-md-12">
                    <div class="section-title text-center">
                         <h4>Ro'yxatdan o'tish</h4>
                         <hr>
                        <?php
                         if(isset($_SESSION['error'])){
                              ?>
                                   <h3 class="alert alert-danger"><?=$_SESSION['error']; ?></h3>
                              <?php
                         }
                         else{

                         }
                        ?>
                    </div><!-- end title -->
               </div><!-- end col -->
          </div><!-- end row -->

          <div class="row section-container">
               <div class="col-md-8 col-md-offset-2">
                    <form action="<?=url.'save'?>" enctype="multipart/form-data" method="post" class="row contact_form">
                         <div class="col-md-6 col-sm-6">
                              <label for="fullname">To'liq ism familiyangiz...</label>
                              <input type="text" id="fullname" name="fullname" class="form-control" placeholder="To'liq ism familiyangiz...">
                         </div>
                         <div class="col-md-6 col-sm-6">
                              <label for="phone">Telefon nomeringiz...</label>
                              <input type="tel" name="phone" id="phone" class="form-control" placeholder="Telefon nomeringiz...">
                         </div>
                         <div class="col-md-6 col-sm-6">
                              <label for="address">Manzilingiz...</label>
                              <input type="text" id="address" name="address" class="form-control" placeholder="Yashash manzilingiz...">
                         </div>
                         <div class="col-md-6 col-sm-6">
                              <label for="img">Rasmingiz...</label>
                              <input type="file" name="img" id="img" class="form-control" placeholder="Rasmingiz...">
                         </div>
                         <div class="col-sm-12">
                              <label for="">Shaxsiy kabinet uchun login va parolni kiriting. Eslatma login  va parol esizdan chiqarib qo'ymang!</label>
                         </div>
                         <div class="col-md-6 col-sm-6">
                              <label for="login">Login...</label>
                              <input type="text" id="login" name="login" class="form-control login" placeholder="Login...">
                         </div>
                         <div class="col-md-6 col-sm-6">
                              <label for="parol">Parol...</label>
                              <input type="password" name="password" id="parol" class="form-control" placeholder="Parol...">
                         </div>
                         <div class="col-md-12 col-sm-6">
                              <label for="info">Iltimos o'zingiz haqingizda ma'lumot kiriting. Qayerda ishlesiz (o'qisiz) darajangiz qande barcha ma'lumotlar!</label>
                              <textarea name="info" id="info" class="form-control" placeholder="Message"></textarea>
                         </div>
                         <div class="col-md-12 col-sm-6">
                              <label for="t">Ustoz bo'lib ro'yxatdan o'tasizmi yoki o'quvchi bo'libmi?</label>
                              <select name="type" class="form-control" id="t">
                                   <option>Ustoz</option>
                                   <option>Shogird</option>
                              </select>
                         </div>
                         <div class="col-md-12 col-sm-6">
                              <button name="send" class="btn btn-primary btn-block">Ma'lumotlar yuborish</button>
                         </div>
                    </form>
               </div>
          </div>
     </div><!-- end container -->
</section><!-- end section-white -->
