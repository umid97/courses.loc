<section class="slider-section">
     <div class="tp-banner-container">
          <div class="tp-banner">
               <ul>
                    <?php foreach(getAllInfo('slider', "DESC") as $r): ?>
                         <li data-transition="slidevertical" data-slotamount="1" data-masterspeed="500" data-thumb="upload/slider_01.jpg"  data-saveperformance="off"  data-title="Slide">
                              <img src="<?=$config['base']['url']?>web/upload/<?=$r['img']?>"  alt="fullslide1"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                              <div class="tp-caption text-center skewfromleft randomrotateout tp-resizeme"
                                   data-x="center"
                                   data-y="165"
                                   data-speed="1000"
                                   data-start="800"
                                   data-easing="Power3.easeInOut"
                                   data-splitin="none"
                                   data-splitout="none"
                                   data-elementdelay="0.1"
                                   data-endelementdelay="0.1"
                                   data-endspeed="1000"
                                   style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;"><img src="<?=$config['base']['url']?>web/upload/slider_logo_01.png" alt="">
                              </div>
                              <div class="tp-caption slider_01 text-center skewfromright randomrotateout tp-resizeme"
                                   data-x="center"
                                   data-y="240"
                                   data-speed="1000"
                                   data-start="1400"
                                   data-easing="Power3.easeInOut"
                                   data-splitin="none"
                                   data-splitout="none"
                                   data-elementdelay="0.1"
                                   data-endelementdelay="0.1"
                                   data-endspeed="1000"
                                   style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;"><?=$r['title']?> <hr>
                              </div>
                              <div class="tp-caption slider_02 text-center skewfromright randomrotateout tp-resizeme"
                                   data-x="center"
                                   data-y="330"
                                   data-speed="1000"
                                   data-start="1400"
                                   data-easing="Power3.easeInOut"
                                   data-splitin="none"
                                   data-splitout="none"
                                   data-elementdelay="0.1"
                                   data-endelementdelay="0.1"
                                   data-endspeed="1000"
                                   style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">
                              </div>
                              <div class="tp-caption slider_03 text-center randomrotateout tp-resizeme"
                                   data-x="center"
                                   data-y="395"
                                   data-speed="1000"
                                   data-start="1400"
                                   data-easing="Power3.easeInOut"
                                   data-splitin="none"
                                   data-splitout="none"
                                   data-elementdelay="0.1"
                                   data-endelementdelay="0.1"
                                   data-endspeed="1000"
                                   style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;"><?=$r['content']?>
                              </div>
                         </li>
                    <?php endforeach; ?>
               </ul>
          </div>
     </div>
</section>

<section class="section-white makepadding">
     <div class="container">
          <div class="relative">
               <div class="background service-absolute">
                    <div class="row">
                         <?php $i = 1; foreach(getAllInfo('services', 'DESC') as $row): ?>
                              <div class="col-md-4 col-sm-6 col-xs-12">
                                   <div class="service-style-1">
                                        <div class="icon-normal wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                                             <a href="<?=url.'services'.$i; ?>"><i class="icon icon-Users"></i></a>
                                        </div><!-- end icon-wrap -->
                                        <div class="title-wrap">
                                             <h5 class="service-title"><?=$row['title']?></h5>
                                             <hr>
                                             <p><?=substr($row['content'], 0, 150).'....'?></p>
                                        </div><!-- end title-wrap -->
                                   </div><!-- end title -->
                              </div><!-- end col -->
                         <?php $i++; endforeach; ?>

                    </div><!-- end row -->
               </div><!-- end background service-absolute -->
          </div><!-- end relative -->
     </div><!-- end container -->


</section><!-- end section-white -->

<section class="section-grey">
     <div class="container">
          <div class="row">
               <div class="col-md-12">
                    <div class="section-title text-center">
                         <h4>Ustozlar</h4>
                         <hr>
                         <p>Ma'lumot olishingiz mumkin!</p>
                    </div><!-- end title -->
               </div><!-- end col -->
          </div><!-- end row -->

          <div id="owl-courses" class="section-container">
               <?php foreach(getAllInfo('teachers', 'DESC') as $row): ?>
                    <div class="course-item">
                         <div class="owl-image">
                              <img style="width: 100%; height: 250px;" src="<?=$config['base']['url']?>web/teachers_img/<?=$row['img']?>" alt="" class="img-responsive">
                              <span class="course-badge"><i class="fa fa-spoon"></i></span>
                         </div><!-- end image -->
                         <div class="course-desc">
                              <h5><?=$row['fullname']?></h5>
                              <span class="meta"><?=$row['phone']?></span>
                              <p><?=substr($row['info'], 0, 150)?></p>
                              <div class="course-big-meta clearfix">
                                   <div class="pull-left">
                                        <a href="<?=url.'more/'.$row['id']?>" class="owl-button">Batafsil</a>
                                   </div><!-- end left -->
                                   <div class="pull-right">
                                        <p><?=$row['address']?></p>
                                   </div><!-- end right -->
                              </div><!-- end course-big-meta -->
                         </div><!-- end desc -->
                    </div><!-- end item -->
               <?php endforeach; ?>

          </div><!-- end owl-carousel -->
     </div><!-- end container -->
</section><!-- end section-white -->



