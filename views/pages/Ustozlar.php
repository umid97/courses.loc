<section class="section-grey">
     <div class="container">
          <div class="row">
               <div class="col-md-12">
                    <div class="section-title text-center">
                         <h4>Ustozlar</h4>
                         <hr>
                         <p>Ma'lumot olishingiz mumkin!</p>
                    </div><!-- end title -->
               </div><!-- end col -->
          </div><!-- end row -->

          <div id="owl-courses" class="section-container">
               <?php foreach(getAllInfo('teachers', 'DESC') as $row): ?>
                    <div class="course-item">
                         <div class="owl-image">
                              <img style="width: 100%; height: 250px;" src="<?=$config['base']['url']?>web/teachers_img/<?=$row['img']?>" alt="" class="img-responsive">
                              <span class="course-badge"><i class="fa fa-spoon"></i></span>
                         </div><!-- end image -->
                         <div class="course-desc">
                              <h5><?=$row['fullname']?></h5>
                              <span class="meta"><?=$row['phone']?></span>
                              <p><?=substr($row['info'], 0, 150)?></p>
                              <div class="course-big-meta clearfix">
                                   <div class="pull-left">
                                        <a href="<?=url.'more/'.$row['id']?>" class="owl-button">Batafsil</a>
                                   </div><!-- end left -->
                                   <div class="pull-right">
                                        <p><?=$row['address']?></p>
                                   </div><!-- end right -->
                              </div><!-- end course-big-meta -->
                         </div><!-- end desc -->
                    </div><!-- end item -->
               <?php endforeach; ?>

          </div><!-- end owl-carousel -->
     </div><!-- end container -->
</section><!-- end section-white -->