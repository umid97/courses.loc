<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<!-- Mirrored from trendingtemplates.com/demos/coursat/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 11 Feb 2020 09:34:37 GMT -->
<head>

     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
     <meta name="description" content="">
     <meta name="author" content="">
     <meta name="keywords" content="">

     <title>Coursat | Learning, Course HTML Template</title>

     <link rel="shortcut icon" href="<?=$config['base']['url']?>web/images/favicon.ico" type="image/x-icon">
     <link rel="apple-touch-icon" href="<?=$config['base']['url']?>web/images/apple-touch-icon.png">
     <link rel="apple-touch-icon" sizes="72x72" href="<?=$config['base']['url']?>web/images/apple-touch-icon-72x72.png">
     <link rel="apple-touch-icon" sizes="114x114" href="<?=$config['base']['url']?>web/images/apple-touch-icon-114x114.png">

     <link rel="stylesheet" type="text/css" href="<?=$config['base']['url']?>web/fonts/font-awesome-4.3.0/css/font-awesome.min.css">
     <link rel="stylesheet" type="text/css" href="<?=$config['base']['url']?>web/css/stroke.css">
     <link rel="stylesheet" type="text/css" href="<?=$config['base']['url']?>web/css/bootstrap.css">
     <link rel="stylesheet" type="text/css" href="<?=$config['base']['url']?>web/css/animate.css">
     <link rel="stylesheet" type="text/css" href="<?=$config['base']['url']?>web/css/carousel.css">
     <link rel="stylesheet" type="text/css" href="<?=$config['base']['url']?>web/css/prettyPhoto.css">
     <link rel="stylesheet" type="text/css" href="<?=$config['base']['url']?>web/css/style.css">

     <!-- COLORS -->
     <link rel="stylesheet" type="text/css" href="<?=$config['base']['url']?>web/css/custom.css">

     <!-- RS SLIDER -->
     <link rel="stylesheet" type="text/css" href="<?=$config['base']['url']?>web/rs-plugin/css/settings.css" media="screen" />

     <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
     <!--[if lt IE 9]>
     <script src="<?=$config['base']['url']?>web/https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
     <script src="<?=$config['base']['url']?>web/https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->

</head>
<body>

<div id="wrapper">

     <div class="topbar clearfix">
          <div class="container">
               <div class="clearfix">
                    <div class="pull-left">
                         <div class="contactwrap text-left">
                              <ul class="list-inline">
                                   <li><i class="fa fa-phone"></i> Call Us : 002 0106 5370701</li>
                                   <li><i class="fa fa-envelope"></i> Email Us : 002 0106 5370701</li>
                                   <li class="dropdown">
                                        <a class="dropdown-toggle" href="#" data-toggle="dropdown"><i class="fa fa-lock"></i> Login / Register</a>
                                        <div class="dropdown-menu">
                                             <form method="post" action="<?=url.'login'?>">
                                                  <div class="form-title">
                                                       <h4>Kabinetga kirish</h4>
                                                       <hr>
                                                  </div>
                                                  <input required class="form-control" type="text" name="username" placeholder="Login...">
                                                  <div class="formpassword">
                                                       <input required class="form-control" type="password" name="password" placeholder="******">
                                                  </div>
                                                 <div>
                                                     <select required name="type" class="form-control" id="">
                                                         <option disabled>Tanlang...</option>
                                                         <option>Ustoz</option>
                                                         <option>Shogird</option>
                                                     </select>
                                                 </div>
                                                  <div class="clearfix"></div>
                                                  <button type="submit" name="send" class="btn btn-block btn-primary">Kirish</button>
                                                  <hr>
                                                  <h4><a href="<?=url.'Registratsiya'?>">Ro'yxatdan o'tish</a></h4>
                                             </form>
                                        </div>
                                   </li>
                              </ul>
                         </div><!-- end contactwrap -->
                    </div><!-- end col -->

               </div><!-- end row -->
          </div><!-- end container -->
     </div><!-- end topbar -->

     <header class="header clearfix">
          <div class="container">
               <nav class="yamm navbar navbar-default">
                    <div class="navbar-header">
                         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                              <span class="sr-only">Toggle navigation</span>
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
                         </button>
                         <a class="navbar-brand" href="<?=url?>"><img src="<?=$config['base']['url']?>web/images/logo.png" alt=""></a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                         <ul class="nav navbar-nav navbar-right">
                              <?php $i = 1; foreach(getAllInfo('category') as $r): ?>
                                  <li class="megamenu"><a class="categorys" href="<?=url.strstr($r['category_name'], ' ', true); ?>"><?=$r['category_name']?></a>
                                  </li>

                              <?php $i++; endforeach; ?>
                              <?php if(isset($_SESSION['fullname_t'])): ?>
                                   <li class=" <?php
                                   if(isset($_GET['view']) && $_GET['view'] == 'cabinet') echo 'active';
                                   ?> megamenu"><a href="<?=url.'Teachers/'; ?>">Cabinet(<?=$_SESSION['fullname_t']?>)</a></li>         <?php elseif(isset($_SESSION['fullname_s'])): ?>
                                  <li class=" <?php
                                  if(isset($_GET['view']) && $_GET['view'] == 'students') echo 'active';
                                  ?> megamenu"><a href="<?=url.'Students/'; ?>">Cabinet(<?=$_SESSION['fullname_s']?>)</a></li>
                              <?php endif; ?>
                         </ul>
                    </div><!--/.nav-collapse -->
               </nav><!-- end nav -->
          </div><!-- end container -->
     </header><!-- end header -->

    <?php
        if(isset($_GET['view']) && $_GET['view'] == 'Bosh'){
             include($config['base']['path'].'views/pages/index.php');
        }
        else{
             include($config['base']['path'].'views/pages/'.$view.'.php');
        }
    ?>

    <footer class="footer">
          <div class="container">
               <div class="row">
                    <div class="col-md-3 col-sm-6">
                         <div class="widget about-widget wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                              <div class="widget-title">
                                   <h4>About</h4>
                              </div><!-- end widget-title -->
                              <p>Suspendisse non augue tincidunt, ullaorper odio vel, tempor risus. In cursus lacus mattis consectetur.</p>
                              <ul class="contact-details">
                                   <li><i class="fa fa-map-marker"></i> 2 AlBahr St, Tanta, AlGharbia, Egypt.</li>
                                   <li><i class="fa fa-phone"></i> +2 01065370701</li>
                                   <li><i class="fa fa-envelope"></i> 7oroof@7oroof.com</li>
                              </ul><!-- end contact-details -->
                         </div><!-- end widget -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6">
                         <div class="widget about-widget wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
                              <div class="widget-title">
                                   <h4>Latest Courses</h4>
                              </div><!-- end widget-title -->
                              <ul class="latest-course">
                                   <li>
                                        <img src="<?=$config['base']['url']?>web/upload/course_mini_01.png" alt="" class="img-responsive alignleft">
                                        <h4><a href="<?=$config['base']['url']?>web/#" title="">New Cooking Lessons</a></h4>
                                        <span>Date : Oct 14, 2014</span>
                                        <span>Created By : <a href="<?=$config['base']['url']?>web/#">Begha</a></span>
                                   </li>
                                   <li>
                                        <img src="<?=$config['base']['url']?>web/upload/course_mini_02.png" alt="" class="img-responsive alignleft">
                                        <h4><a href="<?=$config['base']['url']?>web/#" title="">Web Develop lessons</a></h4>
                                        <span>Date : Oct 14, 2014</span>
                                        <span>Created By : <a href="<?=$config['base']['url']?>web/#">Begha</a></span>
                                   </li>
                              </ul><!-- end latest-course -->
                         </div><!-- end widget -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6">
                         <div class="widget about-widget wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s">
                              <div class="widget-title">
                                   <h4>Popular Tags</h4>
                              </div><!-- end widget-title -->
                              <div class="tagcloud">
                                   <a href="<?=$config['base']['url']?>web/#">Responsive</a>
                                   <a href="<?=$config['base']['url']?>web/#">Modern</a>
                                   <a href="<?=$config['base']['url']?>web/#">Coursat HTML</a>
                                   <a href="<?=$config['base']['url']?>web/#">Fresh</a>
                                   <a href="<?=$config['base']['url']?>web/#">Corporate</a>
                                   <a href="<?=$config['base']['url']?>web/#">Creative</a>
                              </div><!-- end tagline -->
                         </div><!-- end widget -->

                         <div class="widget about-widget wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s">
                              <div class="widget-title">
                                   <h4>Follow Us</h4>
                              </div><!-- end widget-title -->
                              <ul class="social">
                                   <li class="facebook" ><a href="<?=$config['base']['url']?>web/#"><i class="fa fa-facebook"></i></a></li>
                                   <li class="twitter"><a href="<?=$config['base']['url']?>web/#"><i class="fa fa-twitter"></i></a></li>
                                   <li class="google"><a href="<?=$config['base']['url']?>web/#"><i class="fa fa-google-plus"></i></a></li>
                                   <li class="pinterest"><a href="<?=$config['base']['url']?>web/#"><i class="fa fa-pinterest"></i></a></li>
                                   <li class="vimeo"><a href="<?=$config['base']['url']?>web/#"><i class="fa fa-vimeo-square"></i></a></li>
                                   <li class="linkedin"><a href="<?=$config['base']['url']?>web/#"><i class="fa fa-linkedin"></i></a></li>
                                   <li class="dribbble"><a href="<?=$config['base']['url']?>web/#"><i class="fa fa-dribbble"></i></a></li>
                                   <li class="youtube"><a href="<?=$config['base']['url']?>web/#"><i class="fa fa-youtube"></i></a></li>
                                   <li class="rss"><a href="<?=$config['base']['url']?>web/#"><i class="fa fa-rss"></i></a></li>
                              </ul>
                         </div><!-- end widget -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6">
                         <div class="widget about-widget wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                              <div class="widget-title">
                                   <h4>Latest Tweets</h4>
                              </div><!-- end widget-title -->
                              <ul class="latest-tweets">
                                   <li>
                                        <h4><a href="<?=$config['base']['url']?>web/#" title="">@Begha</a> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam.</h4>
                                        <span>2 hours ago</span>
                                   </li>
                                   <li>
                                        <h4><a href="<?=$config['base']['url']?>web/#" title="">@Begha</a> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam.</h4>
                                        <span>2 hours ago</span>
                                   </li>
                              </ul><!-- end latest-course -->
                         </div><!-- end widget -->
                    </div><!-- end col -->
               </div><!-- end row -->
          </div><!-- end container -->
     </footer><!-- end section-white -->

     <section class="copyrights">
          <div class="container">
               <div class="row">
                    <div class="col-md-6 col-sm-12">
                         <p>All Rights Reserved © Coursat Template | Template By <a href="<?=$config['base']['url']?>web/#">TemplateVisual</a></p>
                    </div><!-- end col -->
                    <div class="col-md-6 col-sm-12">
                         <ul class="footer-menu list-inline text-right">
                              <li><a href="<?=$config['base']['url']?>web/#">Home</a></li>
                              <li><a href="<?=$config['base']['url']?>web/#">About</a></li>
                              <li><a href="<?=$config['base']['url']?>web/#">Contact</a></li>
                              <li><a href="<?=$config['base']['url']?>web/#">Purchase</a></li>
                         </ul>
                    </div><!-- end col -->
               </div><!-- end row -->
          </div><!-- end container -->
     </section><!-- end copyrights -->
</div><!-- end wrapper -->

<script src="<?=$config['base']['url']?>web/js/jquery.min.js"></script>
<script src="<?=$config['base']['url']?>web/js/bootstrap.min.js"></script>
<script src="<?=$config['base']['url']?>web/js/retina.js"></script>
<script src="<?=$config['base']['url']?>web/js/wow.js"></script>
<script src="<?=$config['base']['url']?>web/js/carousel.js"></script>
<script src="<?=$config['base']['url']?>web/js/progress.js"></script>
<script src="<?=$config['base']['url']?>web/js/parallax.js"></script>
<script src="<?=$config['base']['url']?>web/js/jquery.prettyPhoto.js"></script>
<script src="<?=$config['base']['url']?>web/js/custom.js"></script>

<!-- SLIDER REV -->
<script src="<?=$config['base']['url']?>web/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="<?=$config['base']['url']?>web/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?=$config['base']['url']?>web/js/ajax.js"></script>
<script>
    /* ==============================================
     SLIDER -->
     =============================================== */
    jQuery('.tp-banner').show().revolution(
        {
            dottedOverlay:"none",
            delay:16000,
            startwidth:1170,
            startheight:665,
            hideThumbs:200,
            thumbWidth:100,
            thumbHeight:50,
            thumbAmount:5,
            navigationType:"none",
            navigationArrows:"solo",
            navigationStyle:"preview3",
            touchenabled:"on",
            onHoverStop:"on",
            swipe_velocity: 0.7,
            swipe_min_touches: 1,
            swipe_max_touches: 1,
            drag_block_vertical: false,
            parallax:"mouse",
            parallaxBgFreeze:"on",
            parallaxLevels:[7,4,3,2,5,4,3,2,1,0],
            keyboardNavigation:"off",
            navigationHAlign:"center",
            navigationVAlign:"bottom",
            navigationHOffset:0,
            navigationVOffset:20,
            soloArrowLeftHalign:"left",
            soloArrowLeftValign:"center",
            soloArrowLeftHOffset:20,
            soloArrowLeftVOffset:0,
            soloArrowRightHalign:"right",
            soloArrowRightValign:"center",
            soloArrowRightHOffset:20,
            soloArrowRightVOffset:0,
            shadow:0,
            fullWidth:"on",
            fullScreen:"off",
            spinner:"spinner4",
            stopLoop:"off",
            stopAfterLoops:-1,
            stopAtSlide:-1,
            shuffle:"off",
            autoHeight:"off",
            forceFullWidth:"off",
            hideThumbsOnMobile:"off",
            hideNavDelayOnMobile:1500,
            hideBulletsOnMobile:"off",
            hideArrowsOnMobile:"off",
            hideThumbsUnderResolution:0,
            hideSliderAtLimit:0,
            hideCaptionAtLimit:0,
            hideAllCaptionAtLilmit:0,
            startWithSlide:0
        });
</script>
<script>

</script>
</body>

<!-- Mirrored from trendingtemplates.com/demos/coursat/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 11 Feb 2020 09:35:23 GMT -->
</html>