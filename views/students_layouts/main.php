<!DOCTYPE html>
<html dir="ltr" lang="en">

<!-- Mirrored from grandetest.com/theme/edumy-html/page-dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 15 Jan 2020 07:25:54 GMT -->
<head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <meta name="keywords" content="academy, college, coursera, courses, education, elearning, kindergarten, lms, lynda, online course, online education, school, training, udemy, university">
     <meta name="description" content="Edumy - LMS Online Education Course & School HTML Template">
     <meta name="CreativeLayers" content="ATFN">
     <!-- css file -->
     <link rel="stylesheet" href="<?=$config['base']['url']?>web/teachers/css/bootstrap.min.css">
     <link rel="stylesheet" href="<?=$config['base']['url']?>web/teachers/css/style.css">
     <link rel="stylesheet" href="<?=$config['base']['url']?>web/teachers/css/dashbord_navitaion.css">
     <!-- Responsive stylesheet -->
     <link rel="stylesheet" href="<?=$config['base']['url']?>web/teachers/css/responsive.css">
     <!-- Title -->
     <title><?=$_SESSION['fullname_s'];?></title>
     <!-- Favicon -->
     <link href="<?=$config['base']['url']?>web/teachers/images/favicon.ico" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
     <link href="<?=$config['base']['url']?>web/teachers/images/favicon.ico" sizes="128x128" rel="shortcut icon" />

     <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
    <script src="<?=$config['base']['url']?>web/teachers/https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="<?=$config['base']['url']?>web/teachers/https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper">
     <div class="preloader"></div>

     <!-- Main Header Nav -->
     <header class="header-nav menu_style_home_one dashbord_pages navbar-scrolltofixed stricky main-menu">
          <div class="container-fluid">
               <!-- Ace Responsive Menu -->
               <nav>
                    <!-- Menu Toggle btn-->
                    <div class="menu-toggle">
                         <a class="navbar-brand" href="<?=url?>"><img src="<?=$config['base']['url']?>web/images/logo.png" alt=""></a>
                         <button type="button" id="menu-btn">
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
                         </button>
                    </div>
                    <div class="navbar_brand float-left dn-smd">
                         <a class="navbar-brand" href="<?=url?>"><img src="<?=$config['base']['url']?>web/images/logo.png" alt=""></a>
                    </div>

                    <ul class="header_user_notif pull-right dn-smd">
                         <li class="user_notif">
                              <strong style="color: white"><?=$_SESSION['fullname_s'];?></strong>
                         </li>
                    </ul>
               </nav>
          </div>
     </header>


     <!-- Our Dashbord Sidebar -->
     <section class="dashboard_sidebar dn-1199">
          <div class="dashboard_sidebars">
               <div class="user_board">
                    <div class="dashbord_nav_list">
                         <ul>
<!--                              <li><a href="--><?//=students?><!--myTeachers"><span class="flaticon-online-learning"></span>Shartnomalarim</a></li>-->
                              <li><a href="<?=students?>message"><span class="flaticon-speech-bubble"></span> Yangi xabarlar <small class="badge badge-danger"><?php
                              $son = count(getConditionData('chat', ['people', 'status'], [$_SESSION['login_s'], 'active']));
                              echo $son;
                                             ?></small></a></li>
                              <li><a href="<?=students?>nomessage"><span class="flaticon-speech-bubble"></span>O'qilgan xabarlar<small class="badge badge-danger"><?php
                                             $son = count(getConditionData('chat', ['people', 'status'], [$_SESSION['login_s'], 'noactive']));
                                             echo $son;
                                             ?></small></a></li>
                              <li><a href="<?=students?>chat"><span class="flaticon-speech-bubble"></span>Xabarlarim<small class="badge badge-danger"><?php
                                             $son = count(getConditionData('students_chat', ['students_login'], [$_SESSION['login_s']]));
                                             echo $son;
                                             ?></small></a></li>
                              <li><a href="<?=students?>teachers"><span class="flaticon-rating"></span> Ustozlar</a></li>
                         </ul>
                         <h4>Account</h4>
                         <ul>
                              <li><a href="<?=students.'logout'?>"><span class="flaticon-logout"></span> Chiqish</a></li>
                         </ul>
                    </div>
               </div>
          </div>
     </section>

     <!-- Our Dashbord -->
     <div class="our-dashbord dashbord">
          <div class="dashboard_main_content">
               <div class="container-fluid">
                    <div class="main_content_container">
                         <?php
                              if($view == 'index'){
                                   include($config['base']['path'].'views/students_pages/message.php');
                              }
                              else{
                                   include($config['base']['path'].'views/students_pages/'.$view.'.php');
                              }
                         ?>
                    </div>
               </div>
          </div>
     </div>
     <a class="scrollToHome" href="<?=$config['base']['url']?>web/teachers/#"><i class="flaticon-up-arrow-1"></i></a>
</div>
<!-- Wrapper End -->
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/jquery-3.3.1.js"></script>
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/jquery-migrate-3.0.0.min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/popper.min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/jquery.mmenu.all.js"></script>
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/ace-responsive-menu.js"></script>
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/chart.min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/chart-custome.js"></script>
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/snackbar.min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/simplebar.js"></script>
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/parallax.js"></script>
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/scrollto.js"></script>
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/jquery-scrolltofixed-min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/jquery.counterup.js"></script>
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/wow.min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/progressbar.js"></script>
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/slider.js"></script>
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/timepicker.js"></script>
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/wow.min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/dashboard-script.js"></script>
<!-- Custom script for all pages -->
<script type="text/javascript" src="<?=$config['base']['url']?>web/teachers/js/script.js"></script>
</body>

<!-- Mirrored from grandetest.com/theme/edumy-html/page-dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 15 Jan 2020 07:25:56 GMT -->
</html>