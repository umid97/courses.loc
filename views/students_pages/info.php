<div class="container">
    <div class="row">
        <div class="col-12">
            <form action="" method="post" enctype="multipart/form-data">
                <label for="files">Faylni yuklang...</label>
                <input type="file" name="files" id="files" class="form-control" >
                <input type="hidden" name="id" value="<?=$_GET['id'];?>" >
                <input name="t_login" type="hidden" value="<?=isGlobalCol('teachers', ['id'], [$_GET['id']], 'AND', 'login');?>" >
                <input type="hidden" name="s_login" value="<?=$_SESSION['login_s'];?>" >
                <label for="telefon">Telefon nomeriz</label>
                <input type="text" name="telefon" class="form-control" required >
                <label for="text">Matni kiriting...</label>
                <textarea name="text" id="text" cols="30" rows="10" class="form-control" ></textarea>
                <br>
                <input type="submit" name="ok" value="Yuborish" class="btn btn-success btn-sm" >
            </form>
        </div>
    </div>
</div>
<?php if(isset($_POST['ok'])): ?>
    <?php
        move_uploaded_file($_FILES['files']['tmp_name'], $config['base']['path'].'web/upload/'.$_FILES['files']['name']);
        if(InsertGlobal('students_chat', ['teachers_login', 'students_login', 'phone', 'text', 'status', 'vaqt', 'files'], [$_POST['t_login'], $_POST['s_login'], $_POST['telefon'], $_POST['text'], 'active', date('Y-m-d H:i:s'), $_FILES['files']['name']])){
            ?>
            <script>
                window.location = '<?=students.'contracts/'.$_POST['id']; ?>'
            </script>
            <?php
        }
        else{
            ?>
            <script>
                window.location = '<?=students.'contracts/'.$_POST['id']; ?>'
            </script>
            <?php
        }
    ?>
<?php endif; ?>
