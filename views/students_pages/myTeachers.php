<div class="row">
     <div class="col-lg-12">
          <nav class="breadcrumb_widgets" aria-label="breadcrumb mb30">
               <h4 class="title float-left">Mening shartnomalarim!</h4>
               <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
               </ol>
          </nav>
     </div>
     <div class="col-lg-12">
          <div class="my_course_content_container">
               <div class="my_course_content mb30">

                    <div class="my_course_content_list">
                         <?php $login = $_SESSION['login_s']; foreach(getConditionData('contract', ['students_login', 'status'], [$login, 'active']) as $row): ?>
                         <div class="mc_content_list">
                              <div class="thumb" style="width: 35%; height: 200px;">
                                   <img style="width: 35%" class="img-whp" src="<?=$config['base']['url']?>web/teachers_img/<?=isGlobalCol('teachers', ['login'], [$row['teachers_login']], 'AND', 'img');?>" alt="t1.jpg">
                                   <div class="overlay">
                                        <ul class="mb0">
                                             <li class="list-inline-item">
                                                  <a class="mcc_view" href="<?=students.'edit/'.isGlobalCol('teachers', ['login'], [$row['teachers_login']], 'AND', 'id')?>">Edit</a>
                                             </li>
                                             <li class="list-inline-item">
                                                  <a class="mcc_view" href="<?=students.'view/'.isGlobalCol('teachers', ['login'], [$row['teachers_login']], 'AND', 'id')?>">View</a>
                                             </li>
                                        </ul>
                                   </div>
                              </div>
                              <div class="details">
                                   <div class="mc_content">

                                        <h5 class="title"><em>O'qutuvchi fish: </em><?=isGlobalCol('teachers', ['login'], [$row['teachers_login']], 'AND', 'fullname')?></h5>
                                       <p style="height: 100px; overflow: auto;" class="subtitle"><?=isGlobalCol('teachers', ['login'], [$row['teachers_login']], 'AND', 'info')?></p>
                                   </div>
                                   <div class="mc_footer">
                                        <ul>
                                             <li>Tel <a href="tel: <?=isGlobalCol('teachers', ['login'], [$row['teachers_login']], 'AND', 'phone');?>" class="alert-link"><?=isGlobalCol('teachers', ['login'], [$row['teachers_login']], 'AND', 'phone');?></a></li>
                                        </ul>
                                        <ul>
                                             <li class="list-inline-item tc_price fn-414">
                                                  <a href="" style="color: red; font-size: 18px;"><?php
                                                       $active = isGlobalCol('contract', ['status'], ['active'], 'AND', 'status');
                                                       if($active == 'active'){
                                                            echo 'Shartnoma kutilmoqda';
                                                       }
                                                       elseif($active == 'inactive'){
                                                           echo 'Shartnoma mavjud!';
                                                       }
                                                       elseif($active == 'noactive'){
                                                            echo 'Bajarib bo`lingan!';
                                                       }
                                                       ?>
                                                  </a>
                                             </li>
                                        </ul>
                                        <hr>
                                        <ul class="mc_review fn-414">
                                             <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                             <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                             <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                             <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                             <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                             <li class="list-inline-item"><a href="#">(5)</a></li>
                                             <li class="list-inline-item tc_price fn-414"><a href="#">$<?=$row['sum']?></a></li>

                                        </ul>

                                   </div>
                              </div>
                         </div>
                         <?php endforeach; ?>

                    </div>
               </div>
          </div>
     </div>
</div>
<div class="row mt10 pb50">
     <div class="col-lg-12">
          <div class="copyright-widget text-center">
               <p class="color-black2">Copyright Edumy © 2019. All Rights Reserved.</p>
          </div>
     </div>
</div>