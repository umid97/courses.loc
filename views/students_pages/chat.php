<div class="row">
     <div class="col-xl-12">
          <div class="row message_container">
               <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 pr0 pl0">
                    <div class="inbox_user_list">

                         <ul>
                              <?php foreach(getConditionData('students_chat', ['students_login', 'status'], [$_SESSION['login_s'], 'active']) as $row): ?>
                                   <li class="contact">
                                        <a href="">
                                             <div class="wrap">
                                                  <span class="contact-status online"></span>
                                                  <img class="img-fluid" src="<?=$config['base']['url']?>web/teachers_img/<?=isGlobalCol('teachers', ['login'], [$row['teachers_login']], 'AND', 'img');?>" alt="s1.jpg"/>
                                                  <div class="meta">
                                                       <h5 class="name"><?=isGlobalCol('teachers', ['login'], [$row['teachers_login']], 'AND', 'fullname');?></h5>
                                                       <p class="preview"><?=$row['text']?></p>
                                                      <p>
                                                          <a class="btn btn-danger btn-sm" href="<?=students.'delitems/'.$row['id']?>">
                                                              <i class="fa fa-trash" aria-hidden="true"></i>
                                                          </a>
                                                      </p>
                                                  </div>
                                             </div>
                                        </a>
                                   </li>
                              <?php endforeach; ?>
                         </ul>
                         <ul>
                              <?php foreach(getConditionData('students_chat', ['students_login', 'status'], [$_SESSION['login_s'], 'noactive']) as $row): ?>
                                   <li class="contact">
                                        <a href="">
                                             <div class="wrap">
                                                  <span class="contact-status bursy"></span>
                                                  <img class="img-fluid" src="<?=$config['base']['url']?>web/teachers_img/<?=isGlobalCol('teachers', ['login'], [$row['teachers_login']], 'AND', 'img');?>" alt="s1.jpg"/>
                                                  <div class="meta">
                                                       <h5 class="name"><?=isGlobalCol('teachers', ['login'], [$row['teachers_login']], 'AND', 'fullname');?></h5>
                                                       <p class="preview"><?=$row['text']?></p>
                                                      <p>
                                                          <a class="btn btn-danger btn-sm" href="<?=students.'delitems/'.$row['id']?>">
                                                              <i class="fa fa-trash" aria-hidden="true"></i>
                                                          </a>
                                                  </div>
                                             </div>
                                        </a>
                                   </li>
                              <?php endforeach; ?>
                         </ul>
                    </div>
               </div>
          </div>
          <div class="row mt50 mb50">
               <div class="col-lg-12">
                    <div class="copyright-widget text-center">
                         <p class="color-black2">Copyright Edumy © 2019. All Rights Reserved.</p>
                    </div>
               </div>
          </div>
     </div>
</div>