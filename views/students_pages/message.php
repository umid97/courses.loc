<div class="row">
     <div class="col-xl-12">
          <div class="row message_container">
               <div class="col-sm-12 col-md-6 col-lg-5 col-xl-4 pr0 pl0">
                    <div class="inbox_user_list">
                         <div class="iu_heading">
                              <div class="candidate_revew_search_box">
                                   <form class="form-inline my-2 my-lg-0">
                                        <input class="form-control mr-sm-2" type="search" placeholder="Serach" aria-label="Search">
                                        <button class="btn my-2 my-sm-0" type="submit"><span class="flaticon-magnifying-glass"></span></button>
                                   </form>
                              </div>
                         </div>
                         <ul>
                              <?php foreach(getConditionData('chat', ['people', 'status'], [$_SESSION['login_s'], 'active']) as $row): ?>
                              <li class="contact">
                                   <a href="<?=students.'message/'.$row['id'];?>">
                                        <div class="wrap">
                                             <span class="contact-status online"></span>
                                             <img class="img-fluid" src="<?=$config['base']['url']?>web/teachers_img/<?=isGlobalCol('teachers', ['login'], [$row['person']], 'AND', 'img');?>" alt="s1.jpg"/>
                                             <div class="meta">
                                                  <h5 class="name"><?=isGlobalCol('teachers', ['login'], [$row['person']], 'AND', 'fullname');?></h5>
                                                  <p class="preview"><?=substr($row['text'], 0, 15)?></p>
                                             </div>
                                        </div>
                                   </a>
                              </li>
                              <?php endforeach; ?>
                         </ul>
                    </div>
               </div>
               <div class="col-sm-12 col-md-6 col-lg-7 col-xl-8 pr0 pl0">
                    <div class="user_heading">
                         <a href="">
                              <div class="wrap">
                                   <span class="contact-status online"></span>

                                   <div class="meta">
                                        <h5 class="name"><?=$_SESSION['fullname_s']?></h5>
                                   </div>
                              </div>
                         </a>
                    </div>
                    <?php if(isset($_GET['id'])): ?>
                         <?php foreach(getConditionData('chat', ['id', 'status'], [$_GET['id'], 'active']) as $r): ?>
                    <div class="container">
                         <div class="row">
                                <?php
                                getUpdate('chat', ['status'], ['noactive'], "WHERE id = '{$_GET['id']}'");
                                ?>

                                   <div class="col-12">
                                        <h4>O'qutuvchi : <mark><?=isGlobalCol('teachers', ['login'], [$r['person']], 'AND', 'fullname')?></mark></h4>
                                   </div>
                                   <div class="col-12">
                                        <h5><?=$r['text']?></h5>
                                        <em><?=$r['vaqt']?></em>
                                   </div>



                         </div>
                    </div>
                    <div class="message_input" style="position: relative; top: 0; left: 0;">
                         <form class="form-inline" method="post" action="" enctype="multipart/form-data">
                              <input type="hidden" name="teachers_log" value="<?=$r['person']?>" >
                             <input type="file" name="files" class="form-control" >
                              <input class="form-control" type="search" placeholder="Enter text here..." name="text" aria-label="Search">
                              <button class="btn" name="ok" type="submit">Send <span class="flaticon-paper-plane"></span></button>
                         </form>
                    </div>
                   <?php endforeach; ?>
                   <?php endif; ?>
               </div>
          </div>
          <div class="row mt50 mb50">
               <div class="col-lg-12">
                    <div class="copyright-widget text-center">
                         <p class="color-black2">Copyright Edumy © 2019. All Rights Reserved.</p>
                    </div>
               </div>
          </div>
     </div>
</div>
<?php if(isset($_POST['ok'])): ?>
    <?php
        if(InsertGlobal('students_chat', ['teachers_login', 'students_login', 'phone', 'text', 'status', 'vaqt', 'files'], [$_POST['teachers_log'], $_SESSION['login_s'], isGlobalCol('students', ['login'], [$_SESSION['login_s']], 'AND', 'phone'), $_POST['text'], 'active', date('Y-m-d'),$_FILES['files']['name']])){
            ?>
            <script>
                alert('Yuborildi!');
                window.location = '<?=students.'message'?>';
            </script>
            <?php
        }
        else{
            ?>
            <script>
                alert('Yuborilmadi!');
                window.location = '<?=students.'message'?>';
            </script>
            <?php
        }
    ?>
<?php endif; ?>
