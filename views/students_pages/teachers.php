<div class="row">
     <div class="col-lg-12">
          <div class="my_course_content_container">
               <div class="my_course_content mb30">

                    <div class="my_course_content_list">
                         <?php $login = $_SESSION['fullname_s']; foreach(getConditionData('teachers', ['status'], ['active']) as $row): ?>
                              <div class="mc_content_list">
                                   <div class="thumb" style="width: 35%; height: 200px;">
                                        <img style="width: 35%" class="img-whp" src="<?=$config['base']['url']?>web/teachers_img/<?=$row['img']?>" alt="t1.jpg">
                                        <div class="overlay">
                                             <ul class="mb0">
                                                  <li class="list-inline-item">
                                                       <a class="mcc_view" href="<?=students.'contracts/'.$row['id'];?>">View</a>
                                                  </li>
                                             </ul>
                                        </div>
                                   </div>
                                   <div class="details">
                                        <div class="mc_content">

                                             <h5 class="title"><?=$row['fullname']?></h5>
                                        </div>
                                        <div class="mc_footer">
                                             <ul>
                                                  <li>Tel <a href="tel: <?=$row['phone']; ?>" class="alert-link"><?=$row['phone']?></a></li>
                                             </ul>
                                             <ul class="mc_review fn-414">
                                                  <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                                  <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                                  <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                                  <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                                  <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                                 <hr>
                                                  <li class="list-inline-item tc_price fn-414"><a href="#"><?=$row['address']?></a></li>

                                             </ul>

                                        </div>
                                   </div>
                              </div>
                         <?php endforeach; ?>

                    </div>
               </div>
          </div>
     </div>
</div>
<div class="row mt10 pb50">
     <div class="col-lg-12">
          <div class="copyright-widget text-center">
               <p class="color-black2">Copyright Edumy © 2019. All Rights Reserved.</p>
          </div>
     </div>
</div>
