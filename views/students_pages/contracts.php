<div class="container">
    <div class="row">
        <table class="table" >
            <a href="<?=students.'teachers'?>" class="btn btn-info btn-sm" style="margin-right: 15px;" >Qaytish</a>
            <a href="<?=students.'info/'.$_GET['id']?>" class="btn btn-info btn-sm" >Matn yozish</a>
            <thead>
            <th>#</th><th>FISH</th><th>ADDRESS</th><th>TELEFON</th><th>Rasm</th><th>Malumotlari: </th>
            </thead>
            <tbody>
                <?php foreach (getConditionData('teachers', ['id'], [$_GET['id']]) as $r): ?>
                <tr>
                    <td><?=$r['id']?></td>
                    <td><?=$r['fullname']?></td>
                    <td><?=$r['address']?></td>
                    <td><?=$r['phone']?></td>
                    <td><img width="50%" src="<?=$config['base']['url'].'web/teachers_img/'.$r['img']?>" alt=""></td>
                    <td>
                        <p align="justify"><?=$r['info']?></p>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>