<div class="row">
     <div class="col-xl-12">
          <div class="row message_container">
               <div class="col-sm-12 col-md-6 col-lg-5 col-xl-4 pr0 pl0">
                    <div class="inbox_user_list">
                         <div class="iu_heading">
                              <div class="candidate_revew_search_box">
                                   <form class="form-inline my-2 my-lg-0">
                                        <input class="form-control mr-sm-2" type="search" placeholder="Serach" aria-label="Search">
                                        <button class="btn my-2 my-sm-0" type="submit"><span class="flaticon-magnifying-glass"></span></button>
                                   </form>
                              </div>
                         </div>
                         <ul>
                              <?php foreach(getConditionData('chat', ['people', 'status'], [$_SESSION['login_s'], 'noactive']) as $row): ?>
                                   <li class="contact">
                                        <a href="<?=students.'nomessage/'.$row['id'];?>">
                                             <div class="wrap">
                                                  <span class="contact-status bursy"></span>
                                                  <img class="img-fluid" src="<?=$config['base']['url']?>web/teachers_img/<?=isGlobalCol('teachers', ['login'], [$row['person']], 'AND', 'img');?>" alt="s1.jpg"/>
                                                  <div class="meta">
                                                       <h5 class="name"><?=isGlobalCol('teachers', ['login'], [$row['people']], 'AND', 'fullname');?></h5>
                                                       <p class="preview"><?=substr($row['text'], 0, 15)?></p>
                                                      <p>
                                                          <a class="btn btn-danger btn-sm" href="<?=students.'delitem/'.$row['id']?>">
                                                              <i class="fa fa-trash" aria-hidden="true"></i>
                                                          </a>
                                                  </div>
                                             </div>
                                        </a>
                                   </li>
                              <?php endforeach; ?>
                         </ul>
                    </div>
               </div>
               <div class="col-sm-12 col-md-6 col-lg-7 col-xl-8 pr0 pl0">
                    <div class="user_heading">
                         <a href="">
                              <div class="wrap">
                                   <span class="contact-status online"></span>

                                   <div class="meta">
                                        <h5 class="name"><?=$_SESSION['fullname_s']?></h5>
                                   </div>
                              </div>
                         </a>
                    </div>
                    <?php if(isset($_GET['id'])): ?>
                         <?php foreach(getConditionData('chat', ['id', 'status'], [$_GET['id'], 'noactive']) as $r): ?>
                              <div class="container">
                                   <div class="row">
                                        <?php
                                        getUpdate('chat', ['status'], ['noactive'], "WHERE id = '{$_GET['id']}'");
                                        ?>

                                        <div class="col-12">
                                             <h4>O'qutuvchi : <mark><?=isGlobalCol('teachers', ['login'], [$r['people']], 'AND', 'fullname')?></mark></h4>
                                        </div>
                                        <div class="col-12">
                                             <h5><?=$r['text']?></h5>
                                             <em><?=$r['vaqt']?></em>
                                        </div>



                                   </div>
                              </div>
                              <div class="message_input" style="position:relative; top: 0; left: 0;">
                                   <form enctype="multipart/form-data" class="form-inline" method="post" action="">
                                        <input type="hidden" name="teachers_log" value="<?=$r['person']?>" >
                                       <input type="file" name="files" class="form-control">
                                        <input class="form-control" type="search" placeholder="Enter text here..." name="text" aria-label="Search">
                                        <button class="btn" name="ok" type="submit">Send <span class="flaticon-paper-plane"></span></button>
                                   </form>
                              </div>
                              <?php if(isset($_POST['ok'])): ?>
                                   <?php
                                    move_uploaded_file($_FILES['files']['tmp_name'], $config['base']['path'].'web/upload/'.$_FILES['files']['name']);
                                   $date = Date('Y-m-d H:i:s');
                                   $tel = isGlobalCol('students', ['login'], [$_SESSION['login_s']], 'AND', 'phone');
                                   if(InsertGlobal('students_chat', ['teachers_login', 'students_login', 'phone', 'text', 'status', 'vaqt', 'files'], [$_POST['teachers_log'], $_SESSION['login_s'], $tel, $_POST['text'], 'active', $date, $_FILES['files']['name']])){
                                        ?>
                              <script>
                                   alert('Yuborildi!');
                                   window.location = '<?=students.'nomessage'?>';
                              </script>
                                        <?php
                                   }
                                   else{
                                        ?>
                              <script>
                                  alert('Yuborilmadi!');
                                  window.location = '<?=students.'nomessage'?>';
                              </script>
                                        <?php
                                   }
                                   ?>
                              <?php endif; ?>
                         <?php endforeach; ?>
                    <?php endif; ?>
               </div>
          </div>
     </div>
</div>