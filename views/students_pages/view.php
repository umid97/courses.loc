<div class="row">
     <div class="col-lg-12">
          <table class="table table-hover table-bordered">
               <thead>
                    <th>#</th><th>Ustoz fish:</th><th>Telefon nomer: </th><th>Ustoz manzil</th><th>Rasm</th><th>Berilgan vaqt: </th><th></th>
               </thead>
               <tbody>
                    <tr>
                         <?php foreach(getConditionData('teachers', ['id'], [$_GET['id']]) as $row): ?>
                              <td>
                                   <?=$row['id']?>
                              </td>
                              <td>
                                   <?=$row['fullname']?>
                              </td>
                              <td>
                                   <?=$row['phone']?>
                              </td>
                              <td>
                                   <?=$row['address']?>
                              </td>
                              <td>
                                   <img style="width: 25%" class="img-whp" src="<?=$config['base']['url']?>web/teachers_img/<?=isGlobalCol('teachers', ['id'], [$_GET['id']], 'AND', 'img');?>" alt="t1.jpg">
                              </td>
                              <td>
                                   <strong><?=isGlobalCol('contract', ['students_login'], [$_SESSION['login_s']], 'AND', 'vaqt')?></strong>
                              </td>
                              <td>
                                   <a href="<?=students.'bekor'?>" class="btn btn-success btn-sm">Bekor qilish</a>
                              </td>
                         <?php endforeach; ?>
                    </tr>
               </tbody>
          </table>
     </div>
</div>