<table class="table table-hover">
    <thead>
        <th>O'qutuvchi fish: </th>
        <th>O'qutuvchi telefon nomer: </th>
        <th>O'qutuvchi address: </th>
        <th>O'qutuvchi rasm: </th>
        <th>O'qutuvchi ma'lumot: </th>
        <th>O'qutuvchi login: </th>
        <th>O'qutuvchi parol: </th>
        <th>O'qutuvchi status</th>
    </thead>
    <tbody>
        <?php $son = isset($_GET['id']) ? $_GET['id'] : 0; foreach(getPage('students', $son) as $r): ?>
            <tr class="<?php
            if($r['status'] == 'active'){
                 echo 'alert alert-warning';
            }
            elseif($r['status'] == 'inactive'){
                 echo 'alert alert-danger';
            }
            elseif($r['status'] == 'noactive'){
                 echo 'alert alert-success';
            }
            ?>">
                <td>
                     <a href="<?=admin.'students/'.$r['id'];?>" class="text-primary"><?=$r['fullname']?></a>
                </td>

                 <td><?=$r['phone'];?></td>

                 <td><?=$r['address'];?></td>

                  <td width="15%">
                      <img src="<?=$config['base']['url'].'web/students_img/'.$r['img']?>" alt="">
                  </td>

                  <td><?=$r['info'];?></td>

                  <td><?=$r['login'];?></td>
                 <td><?=$r['password']?></td>
                 <td><?=$r['status']?></td>
             </tr>



        <?php endforeach; ?>
    </tbody>
</table>
