<table class="table table-hover">
     <thead>
     <th>O'qutuvchi fish: </th>
     <th>O'qutuvchi telefon nomer: </th>
     <th>O'qutuvchi address: </th>
     <th>O'qutuvchi rasm: </th>
     <th>O'qutuvchi malumoti: </th>
     <th>O'qutuvchi login: </th>
     <th>O'qutuvchi parol: </th>
     <th>O'qutuvchi status: </th>
     <th></th>
     </thead>
     <tbody>
     <?php $son = isset($_GET['id']) ? $_GET['id'] : 0; foreach(getPage('teachers', $son) as $r): ?>
          <tr class="<?php
          if($r['status'] == 'active'){
               echo 'alert alert-warning';
          }
          elseif($r['status'] == 'inactive'){
               echo 'alert alert-danger';
          }
          elseif($r['status'] == 'noactive'){
               echo 'alert alert-success';
          }
          ?>">
               <td>
                    <a href="<?=admin.'teach/'.$r['id'];?>" class="text-primary"><?=$r['fullname']?></a>
               </td>

               <td><?=$r['phone']?></td>

               <td><?=$r['address']?></td>

               <td>
                   <img src="<?=$config['base']['url'].'web/teachers_img/'.$r['img']?>" class="img-thumbnail" alt="">
               </td>

               <td><?=substr($r['info'], 0, 250).'...';?></td>

               <td><?=$r['login']?></td>
               <td><?=$r['password']?></td>
               <td><?=$r['status']?></td>
               <td>

               </td>
          </tr>



     <?php endforeach; ?>
     </tbody>
</table>
