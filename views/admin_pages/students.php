<?php foreach(getConditionData('students', ['id'], [$_GET['id']]) as $r):?>
     <table class="table table-bordered table-hover">
          <thead>
          <th>#</th><th>Fish</th><th>Telefon</th><th>Manzil</th><th>Rasm</th><th>login</th><th>Parol</th><th>Xolati</th><th></th>
          </thead>
          <tbody>
          <tr>
              <?php $_SESSION['id'] = $r['id']?>
               <td><?=$r['id']?></td>
               <td><?=$r['fullname']?></td>
               <td><?=$r['phone']?></td>
               <td><?=$r['address']?></td>
               <td>
                    <img src="<?=$config['base']['url'].'web/students_img/'.$r['img']?>" class="img-rounded img-thumbnail" alt="">
               </td>
               <td>
                    <?=$r['login']?>
               </td>
               <td>
                    <?=$r['password']?>
               </td>
               <td>
                    <?=$r['status']?>
               </td>
               <td>
                    <a class="btn btn-danger btn-sm" href="<?=admin.'sdelete/'.isGlobalCol('students', ['id'], [$r['id']], 'AND', 'id')?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
               </td>
          </tr>
          </tbody>
     </table>
     <div class="col-6">
          <form action="" method="post" >
               <input type="hidden" name="admin" value="admin" >
               <input type="hidden" name="person" value="<?=$r['login']?>" >
               <input type="hidden" name="id" value="<?=$r['id']?>" >
               <label for="text">Matn yuboring...</label>
               <textarea name="text" id="text" cols="30" rows="10" class="form-control"></textarea><br>
               <input type="submit" name="ok" class="btn btn-success btn-sm" value="Yuborish" >
          </form>
          <?php if(isset($_POST['ok'])): ?>
               <?php
               if(InsertGlobal('chat', ['people', 'person', 'text', 'vaqt', 'status', 'category'], [$_POST['admin'], $_POST['person'], $_POST['text'], date('Y-m-d'), 'active', 'students'])){
                    ?>
                    <script>
                        alert('Yuborildi!');
                        window.location = '<?=admin.'students/'.$_POST["id"];?>';
                    </script>
               <?php
               }
               else{
               ?>
                    <script>
                        alert('Yuborilmadi!');
                        window.location = '<?=admin.'teachers/'.$_POST["id"];?>';
                    </script>
                    <?php
               }
               ?>
          <?php endif; ?>
     </div>
     <div class="col-6">
          <table class="table table-hover table-bordered">
               <tr>
                    <td colspan="6"><h5 class="alert alert-danger">
                              Mening xabarlarim!
                         </h5></td>
               </tr>
               <?php foreach(getConditionData('chat', ['people','person'], ['admin',$r['login']]) as $r): ?>
                    <tr>
                         <td>
                              <?=$r['id']?>
                         </td>
                         <td><?=$r['person']?></td>
                         <td><?=$r['text']?></td>
                         <td><em><?=$r['vaqt']?></em></td>
                         <td><?php
                              if($r['status'] == 'active'){
                                   echo "O'qilmadi!";
                              }
                              elseif($r['status'] == 'noactive'){
                                   echo "O'qilmadi!";
                              }

                              ?></td>
                         <td>
                              <a class="btn btn-danger btn-sm" href="<?=admin.'chatdeletes/'.$r['id']?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                         </td>
                    </tr>
               <?php endforeach; ?>
              <?php foreach(getConditionData('chat', ['people','person'], [$r['login'], 'admin']) as $r): ?>
                  <tr>
                      <td>
                          <?=$r['id']?>
                      </td>
                      <td><?=$r['person']?></td>
                      <td><?=$r['text']?></td>
                      <td><em><?=$r['vaqt']?></em></td>
                      <td><?php
                          if($r['status'] == 'active'){
                              echo "O'qilmadi!";
                          }
                          elseif($r['status'] == 'noactive'){
                              echo "O'qilmadi!";
                          }

                          ?></td>
                      <td>
                          <a class="btn btn-danger btn-sm" href="<?=admin.'chatdelete/'.$r['id']?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                      </td>
                  </tr>
              <?php endforeach; ?>
          </table>
     </div>
<?php endforeach; ?>

