<?php if(isset($_POST['login'])): ?>
     <?php
     echo "<pre>";
     print_r($_POST);
     echo "</pre>";
     getAdminSession($_POST['login'], $_POST['parol']);
     header('Location: ../admin');
     ?>
<?php endif; ?>

<!doctype html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta name="viewport"
           content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
     <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <title>Document</title>
     <link rel="stylesheet" href="<?=$config['base']['url'].'web/'?>admin/css/bootstrap.min.css">
     <link rel="stylesheet" href="<?=$config['base']['url'].'web/'?>admin/css/style.css">
     <!-- Responsive stylesheet -->
     <link rel="stylesheet" href="<?=$config['base']['url'].'web/'?>admin/css/responsive.css">
     <!-- Title -->
     <title>Edumy - LMS Online Education Course & School HTML Template</title>
     <!-- Favicon -->
     <link href="<?=$config['base']['url'].'web/'?>admin/images/favicon.ico" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
     <link href="<?=$config['base']['url'].'web/'?>admin/images/favicon.ico" sizes="128x128" rel="shortcut icon" />
     <style>
          body{
               background: #1d78cb;
               color: white;
          }
          h3{
               color: white;
          }
          a{
               color: white !important;
          }
     </style>
</head>
<body>
<div class="container">
     <div class="row">
          <div class="col-4"></div>
          <div class="col-4">
               <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                         <div class="login_form">
                              <?php if(isset($_SESSION['error_admin'])): ?>
                                   <h3 class="alert alert-warning">
                                        <?php
                                        echo $_SESSION['error_admin'];
                                        ?>
                                   </h3>
                              <?php endif; ?>
                              <form action="<?=$_SERVER['PHP_SELF'];?>" method="post">
                                   <div class="heading">
                                        <h3 class="text-center">Kabintega o'tish uchun login va parolni kiriting.</h3>
                                   </div>
                                   <div class="form-group">
                                        <input type="text" autocomplete="off" name="login" required class="form-control" id="" placeholder="Login">
                                   </div>
                                   <div class="form-group">
                                        <input type="password" required name="parol" class="form-control" id="" placeholder="Password">
                                   </div>

                                   <button type="submit" class="btn btn-log btn-block btn-thm2">Login</button>
                                   <hr>

                              </form>
                         </div>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                         <div class="sign_up_form">
                              <div class="heading">
                                   <h3 class="text-center">Yangi kabinet ochish</h3>
                              </div>
                              <form action="<?=url_home.'reg'; ?>" method="post">
                                   <div class="form-group">
                                        <input type="text" name="username" class="form-control" id="exampleInputName1" placeholder="To'liq ism sharif">
                                   </div>
                                   <div class="form-group">
                                        <input type="date" name="date" class="form-control" id="" placeholder="Tug'ilgan yil.">
                                   </div>
                                   <div class="form-group">
                                        <input type="email" name="email" class="form-control" id="exampleInputEmail2" placeholder="Email Address">
                                   </div>
                                   <div class="form-group">
                                        <input type="text" name="login" class="form-control" id="" placeholder="Login">
                                   </div>
                                   <div class="form-group">
                                        <input type="password" name="parol" class="form-control" id="" placeholder="Parol">
                                   </div>
                                   <div class="form-group">
                                        <input type="tel" name="phone" class="form-control" id="" placeholder="Telefon nomer">
                                   </div>
                                   <div class="form-group">
                                        <select class="form-control" name="category" id="">
                                             <?php foreach(getAllSelect('category', " WHERE fan = 'fan' ") as $row): ?>
                                                  <option value="<?=$row['id']?>"><?=$row['name']?></option>
                                             <?php endforeach; ?>
                                        </select>
                                   </div>
                                   <div class="form-group custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="auth" required id="exampleCheck2">
                                        <label class="custom-control-label" for="exampleCheck2">Want to become an instructor?</label>
                                   </div>
                                   <button type="submit" class="btn btn-log btn-block btn-thm2">Register</button>
                                   <hr>
                                   <div class="row mt40">
                                        <div class="col-lg">
                                             <button type="submit" class="btn btn-block color-white bgc-fb"><i class="fa fa-facebook float-left mt5"></i> Facebook</button>
                                        </div>
                                        <div class="col-lg">
                                             <button type="submit" class="btn btn-block color-white bgc-gogle"><i class="fa fa-google float-left mt5"></i> Google</button>
                                        </div>
                                   </div>
                              </form>
                         </div>
                    </div>
               </div>
          </div>
          <div class="col-4"></div>
     </div>
</div>
<!-- Wrapper End -->
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/jquery-3.3.1.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/jquery-migrate-3.0.0.min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/popper.min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/jquery.mmenu.all.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/ace-responsive-menu.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/isotop.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/snackbar.min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/simplebar.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/parallax.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/scrollto.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/jquery-scrolltofixed-min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/jquery.counterup.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/wow.min.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/progressbar.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/slider.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/timepicker.js"></script>
<script type="text/javascript" src="<?=$config['base']['url'].'web/'?>admin/js/script.js"></script>
</body>
</html>