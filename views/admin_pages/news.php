<div class="container">
    <div class="row">
        <div class="col-12">
            <table class="table table-hover">
                <thead>
                <th>#</th><th>Sarlavhasi</th><th>Matn</th><th>Fayl</th><th>status</th><th>Vaqt</th>
                </thead>
                <tbody>
            <?php $son = (isset($_GET['id'])) ? $_GET['id'] : 0; foreach (getPage('ad', $son) as $r): ?>
                    <tr>
                        <td><?=$r['id']?></td>
                        <td><?=$r['title']?></td>
                        <td><?=$r['content']?></td>
                        <td><a href="<?=$config['base']['url'].'web/files/'.$r['file']?>" class="btn btn-warning btn-sm"><?=$r['file']?></a></td>
                        <td><?=$r['status']?></td>
                        <td><?=$r['vaqt']?></td>
                        <td>
                            <a class="btn btn-danger btn-sm" href="<?=admin.'deladmin/'.$r['id']?>">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
            <?php endforeach; ?>
                </tbody></table>
            <?=Pagination('ad', admin, 'news');?>
        </div>
        <div class="col-12">
            <form enctype="multipart/form-data" action="" method="post" >
                <label for="title">Sarlavha</label>
                <input type="text" name="title" class="form-control" >
                <br>
                <label for="content">Matn</label>
                <textarea name="content" id="content" cols="30" class="form-control" rows="10"></textarea>
                <br>
                <label for="file">Faylni yuklang</label>
                <input type="file" name="file" class="form-control" >
                <br>
                <input type="submit" name="ok" value="Yuborish" class="btn btn-success btn-sm" >
            </form>
        </div>
    </div>
</div>
<?php if(isset($_POST['ok'])): ?>
    <?php
        move_uploaded_file($_FILES['file']['tmp_name'], $config['base']['path'].'web/files/'.$_FILES['file']['name']);
        if(InsertGlobal('ad', ['title', 'content', 'file', 'status', 'vaqt'], [$_POST['title'], $_POST['content'], $_FILES['file']['name'], 'active', date('Y-m-d')])){
            ?>
            <script>
                window.location = '<?=admin.'news';?>';
            </script>
            <?php
        }
        else{
            ?>
            <script>
                window.location = '<?=admin.'news';?>';
            </script>
            <?php

    }

    ?>
<?php endif; ?>
