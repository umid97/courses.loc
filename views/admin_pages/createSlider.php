<div class="container">
    <div class="row">
        <div class="col-12">
            <form method="post" action="" enctype="multipart/form-data">
                <label for="title">Sarlavha</label>
                <input required type="text" name="title" id="title" class="form-control">
                <label for="content">Matn</label>
                <textarea required name="text" id="text" cols="30" class="form-control" rows="10"></textarea>
                <label for="file">Rasm yuklang!</label>
                <input required type="file" name="file" id="file" class="form-control" ><br>
                <input type="submit" name="ok" class="btn btn-success" value="Saqlash" >
            </form>
            <?php if(isset($_POST['ok'])): ?>
                <?php
                echo '<pre>';
                print_r($_POST);
                echo '</pre>';
                move_uploaded_file($_FILES['file']['tmp_name'], $config['base']['path'].'web/upload/'.$_FILES['file']['name']);
                if(InsertGlobal('slider', ['title', 'content', 'img'], [$_POST['title'], $_POST['content'], $_FILES['file']['name']])){
                    ?>
                    <script>
                        window.location = '<?=admin.'slider';?>';
                    </script>
                    <?php
                }
                else{
                    ?>
                    <script>
                        window.location = '<?=admin.'createSlider';?>';
                    </script>
                    <?php
                }
                ?>
            <?php endif; ?>
        </div>
    </div>
</div>