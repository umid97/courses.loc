<?php
     session_start();
     include '../config.php';
     include '../libs/db.php';
     include '../libs/widgets.php';

     $view = $_GET['view'] ?? 'index';
     if(isset($_SESSION['fullname_s'])){
          include($config['base']['path'].'views/students_layouts/main.php');
     }
     else{
          ?>
         <script>
             window.location = '<?=reg?>';
         </script>
          <?php
     }
