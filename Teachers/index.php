<?php
    session_start();
     include '../config.php';
     include '../libs/db.php';
     include '../libs/widgets.php';

     $view = $_GET['view'] ?? 'index';

     if(isset($_SESSION['fullname_t'])){
          include($config['base']['path'].'views/teachers_layouts/main.php');
     }
     else{
          ?>
          <script>
               window.location = '<?=reg?>';
          </script>
          <?php
     }
