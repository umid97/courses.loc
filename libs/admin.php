<?php
     // admin uchun sessiya
     function getAdminSession($login, $parol){
          if(getAdmin($login, $parol)){
               $_SESSION['login_admin'] = $login;
               $_SESSION['parol_admin'] = $parol;
               return true;
          }
          else{
               $_SESSION['error'] = "Login yoki Parol xato!";
               return false;
          }
     }

     // kirish uchun tekshirish
     function isAuthAdmin(){
          if(isset($_SESSION['login_admin']) && isset($_SESSION['parol_admin'])){
               return true;
          }
          else{
               return false;
          }
     }

     //adminkadan chiqish
     function logout(){
          unset($_SESSION['login_admin']);
          unset($_SESSION['parol_admin']);
          return true;
     }