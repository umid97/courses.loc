<?php
     function connection(){
          global $config;
          $db = new mysqli($config['db']['host'], $config['db']['username'],$config['db']['userpassword'],$config['db']['dbname']);
          $db->query('SET NAMES utf8');
          return $db;
     }

     //massiv
     function getArray($row){
          $array = [];
          $i = 0;
          while ($r = $row->fetch_array()){
               $array[$i] = $r;
               $i++;
          }
          return $array;
     }

     // tablePrefix
     function getTablePrefix($name){
          global $config;
          return $config['db']['tablePrefix'].$name;
     }

     // global insert
     function InsertGlobal($tablename, $array1, $array2){
          $db = connection();
          if(is_array($array1) && is_array($array2)){
               if(count($array1) === count($array2)){
                    $query = "INSERT INTO ".getTablePrefix($tablename)."(";
                    for($i = 0; $i < count($array1); $i++){
                         $query .= $array1[$i].', ';
                    }
                    $query = rtrim($query, ', ');
                    $query .= ") VALUES (";
                    for($i = 0; $i < count($array2); $i++){
                         $query .= "'".strip_tags(addslashes($array2[$i]))."',";
                    }
                    $query = rtrim($query, ',');
                    $query .= ")";
                    $sql = $db->query($query);
                    if($sql)
                         return true;
                    return false;
               }
               else{
                    return false;
               }
          }
          else{
               return false;
          }
     }

     // global tekshirish
     function IsAuth($tablename, $array1, $array2, $condition=null){
          $db = connection();
          if(is_array($array1) && is_array($array2)){
               if(count($array1) === count($array2)){
                    $query = "SELECT * FROM ".getTablePrefix($tablename)." WHERE ";
                    for($i = 0; $i < count($array1); $i++){
                         if(count($array1) - 1 === $i ){
                              $query .= $array1[$i].' = '."'".strip_tags(addslashes($array2[$i]))."' ";
                         }
                         else{
                              $query .= $array1[$i].' = '."'".strip_tags(addslashes($array2[$i]))."' ".$condition." ";
                         }
                    }
                    $sql = $db->query($query);
                    $r = $sql->num_rows;
                    if($r > 0)
                         return true;
                    return false;
               }
               else{

               }
          }
          else{

          }
     }

     // sessiyalarni o'chirish
     function deleteSession(){
          if(isset($_SESSION['login']))
               unset($_SESSION['login']);
          session_destroy();
          return true;
     }

     // global qiymat qaytaruvchi
     function isGlobalCol($tablename, $array1, $array2, $condition=null, $column){
          $db = connection();
          if(is_array($array1) && is_array($array2)){
               if(count($array1) === count($array2)){
                    $query = "SELECT * FROM ".getTablePrefix($tablename)." WHERE ";
                    for($i = 0; $i < count($array1); $i++){
                         if(count($array1) - 1 === $i ){
                              $query .= $array1[$i].' = '."'".strip_tags(addslashes($array2[$i]))."' ";
                         }
                         else{
                              $query .= $array1[$i].' = '."'".$array2[$i]."' ".$condition." ";
                         }
                    }
                    $sql = $db->query($query);
                    $name = $sql->fetch_array();
                    return $name[$column];
               }
               else{

               }
          }
          else{

          }
     }

     // barcha malumotlarni olish
     function getAllInfo($tablename, $sort='ASC'){
          $db = connection();
          $sql = $db->query("SELECT * FROM ".getTablePrefix($tablename)." ORDER BY id {$sort} ");
          return getArray($sql);
     }

     // adminni tekshirish
     function getAdmin($login, $parol){
          $db = connection();

          $login = strip_tags(addslashes($login));
          $parol = strip_tags(addslashes($parol));

          $sql = $db->query("SELECT * FROM courses_admin WHERE login = '{$login}' AND password = '{$parol}'");
          $r = $sql->num_rows;
          if($r > 0)
               return true;
          return false;
     }



     // malumotlarni massivga yig'ish
     function getArrayInfo($col_name, $arr){
          $db = connection();
          $sql = $db->query("SELECT {$col_name} FROM ".getTablePrefix($arr));
          $array = [];
          $i = 0;
          $row = getArray($sql);
          foreach($row as $r){
               $array[$i] = $r['name'];
               $i++;
          }
          return $array;
     }

     // shart bilan ma'lumotlarni olish
     function getConditionData($tablename, $array1, $array2){
          $db = connection();
          if(is_array($array1) && is_array($array2)){
               if(count($array1) === count($array2)){
                    $query = "SELECT * FROM ".getTablePrefix($tablename)." WHERE ";
                    for($i = 0; $i < count($array1); $i++){
                         if(count($array1) - 1 === $i ){
                              $query .= $array1[$i].' = '."'".strip_tags(addslashes($array2[$i]))."' ";
                         }
                         else{
                              $query .= $array1[$i].' = '."'".strip_tags(addslashes($array2[$i]))."' AND ";
                         }
                    }
                    $query = rtrim($query, 'AND ');
                    $sql = $db->query($query);
                    return getArray($sql);
               }
               else{

               }
          }
          else{

          }
     }

     function fulllogout(){
          session_destroy();
          return true;
     }


     function getUpdate($name, $array1, $array2, $condition){
          $db = connection();
          if(is_array($array1) && is_array($array2)){
               if(count($array1) === count($array2)){
                    $query = "UPDATE ".getTablePrefix($name)." SET ";
                    for($i = 0; $i < count($array1); $i++){
                         $query .= $array1[$i].' = '."'".strip_tags(addslashes($array2[$i]))."',";
                    }
                    $query = rtrim($query, ',');
                    $query .= ' '.$condition;
                    $sql = $db->query($query);
                    if($sql)
                         return true;
                    return false;
               }
               else{

               }
          }
          else{

          }
     }

     function Delete($name, $id){
         $db = connection();
         $sql = $db->query("DELETE FROM ".getTablePrefix($name)." WHERE id = $id");
         if($sql)
             return true;
         return false;
     }