<?php
     function getArrayExplode($arr, $char){
          $d = explode($char, $arr);
          $name = 'img'.time().'.'.$d[count($d)-1];
          return $name;
     }

     function getPage($name, $limit=0, $sort='DESC'){
          $db = connection();
          $sql = $db->query("SELECT * FROM ".getTablePrefix($name)." ORDER BY id $sort limit $limit,10");
          return getArray($sql);
     }

     function getCount($name){
          $db = connection();
          $sql = $db->query("SELECT * FROM ".getTablePrefix($name));
          return count(getArray($sql));
     }

     function Pagination($name, $admin=admin, $index='index'){
          $c = ceil(getCount($name) / 10);
          $son = 0;
          $str = '<nav aria-label="Page navigation example"><ul class="pagination justify-content-center">';
          for($i = 1; $i <= $c; $i++){
               $str .= '<li class="page-item"><a class="page-link" href="'.$admin.$index.'/'.$son.'">'.$i.'</a></li>';
               $son += 10;
          }
          $str .= '</ul></nav>';
          return $str;
     }