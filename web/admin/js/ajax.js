$(function(){
    $('#month').change(function(){
        let month = $(this).val();
        $.ajax({
            data: {month: month, login: $('#login').val()},
            type: 'GET',
            url: 'http://localhost/backend_1/amaliyot/ajax/tolov.php',
            success: function(res){
                if(res){
                    $('#month').css({
                        'border': '2px solid red'
                    });
                    $('#month').val(' ');
                    $('#month').focus();
                    $('#month').attr('placeholder', 'Bu oy tolangan!')
                }
                else{
                    $('#month').css({
                        'border': 'none'
                    });
                }
            },
            error: function(e){
                alert('Xatolik!');
            }
        })
    });

    $('#teach').click(function(e){
        e.preventDefault();
        $.ajax({
            url: 'http://localhost/backend_1/amaliyot/ajax/generate.php',
            type: 'GET',
            success: function(e){

                e = e.split(' ');
                console.log(e);
                $('#l').val(e[0]);
                $('#p').val(e[1]);
            }
        });
    })

    $('#span').hover(function(){
        $('#p').attr('type','text');
        $(this).html('<i class="fa fa-eye" aria-hidden="true"></i>');
    },
        function(){
            $('#p').attr('type','password');
            $(this).html('<i class="fa fa-eye-slash" aria-hidden="true"></i>');
        }
        )
})